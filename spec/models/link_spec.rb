require 'spec_helper'

describe Link do
  describe "should have a creator and assigned sub-reddit" do
    it { should belong_to(:creator) }
    it { should belong_to(:sub) }
  end
end

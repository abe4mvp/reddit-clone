require 'spec_helper'

describe Sub do
  describe "should have a moderator and links" do
    it { should have_many(:links)}
    it { should belong_to(:moderator)}
  end
end


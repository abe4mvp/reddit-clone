require 'spec_helper'

describe User do
  describe "stores user data" do
    it "should have a username" do
      expect(FactoryGirl.build(:user, :username => nil)).to_not be_valid
    end

    it "should not store password" do
      FactoryGirl.create(:user)
      expect(User.last.password).to be_nil
    end

    it "should accept a session token" do
      expect(FactoryGirl.build(:user)).to respond_to(:reset_token)

    end

    it "should not allow access to passw0rd_hash" do
      expect{ User.new(password_hash: "hackerthingy") }
      .to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe "finds users by credentials" do
    it "should find a user that enters valid credentials" do
      FactoryGirl.create(:user, :username => "Bo", :password => "password")
      expect(User.find_by_credentials("Bo", "password")).to be_instance_of(User)
    end
    it "should not find non-existent user" do
      expect(User.find_by_credentials("b", "password")).to be_nil
    end
    it "should not find users when password is incorrect" do
      FactoryGirl.create(:user, :username => "Bo", :password => "password")
      expect(User.find_by_credentials("Bo", "passsword")).to be_nil
    end
  end

  describe "should have assocations" do
    it { should have_many(:links) }
    it { should have_many(:moderated_subs) }
  end

end







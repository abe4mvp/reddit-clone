Reddit::Application.routes.draw do
  resources :subs do
    resources :links
  end

  root to: "subs#index"
end



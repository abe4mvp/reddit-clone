class CreateSubs < ActiveRecord::Migration
  def change
    create_table :subs do |t|
      t.string :moderator_id
      t.string :name

      t.timestamps
    end

    add_index :subs, :moderator_id
  end
end

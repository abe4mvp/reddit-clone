class AddColumnsToLinks < ActiveRecord::Migration
  def change
    add_column :links, :title, :string
    add_column :links, :url, :string
    add_column :links, :creator_id, :integer
    add_column :links, :sub_id, :integer

    add_index :links, :creator_id
    add_index :links, :sub_id
  end
end

module ApplicationHelper
  def save_model!(model, redirect_url = nil)

    if model.save # change to snake case
      redirect_to  redirect_url || model #send("#{model.class.to_s.downcase}_url", model )
    else
      flash[:errors] = model.errors.full_messages
      render :new
    end
  end

end

class LinksController < ApplicationController

  def show
    @link = Link.find(params[:id])
  end

  def create
    @link = Sub.find(params[:sub_id]).links.new(params[:link])
    save_model!(@link, sub_links_url(params[:sub_id])
  end
end

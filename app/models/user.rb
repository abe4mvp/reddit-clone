require 'bcrypt'
class User < ActiveRecord::Base
  attr_accessible :username, :password
  attr_reader :password
  validates :username, :password, :presence => true
  validates :password, :length => { :minimum => 6 }
  validates :username, :uniqueness => true

  has_many(
  :links,
  class_name: "Link",
  foreign_key: :creator_id,
  primary_key: :id
  )


  has_many(
    :moderated_subs,
    :class_name => "Sub",
    :foreign_key => :moderator_id,
    :primary_key => :id
  )

  def password=(password)
    @password = password
    self.password_hash = BCrypt::Password.create(password)
  end

  def password_is?(password)
    BCrypt::Password.new(self.password_hash).is_password?(password)
  end

  def reset_token
    self.token = SecureRandom.urlsafe_base64(16)
    self.save!
    self.token
  end


  def self.find_by_credentials(username, password)
    user = User.find_by_username(username)
    if user && user.password_is?(password)
      user
    else
      nil
    end
  end


end

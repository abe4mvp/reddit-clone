class Sub < ActiveRecord::Base
   attr_accessible :moderator_id, :name, :creator_id

   has_many :links

   belongs_to(
   :moderator,
   class_name: "User",
   foreign_key: :moderator_id,
   primary_key: :id)


end

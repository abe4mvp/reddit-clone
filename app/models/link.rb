class Link < ActiveRecord::Base
  attr_accessible :title, :url, :creator_id, :sub_id
  validates :title, :url, :creator_id, :sub_id, :presence => true

  belongs_to :sub
  belongs_to(
    :creator,
    :class_name => "User",
    :foreign_key => :creator_id,
    :primary_key => :id
  )
end
